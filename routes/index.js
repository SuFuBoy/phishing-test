var express = require('express');
var path = require('path');
var router = express.Router();

const getQuestions = require('./../scripts/getQuestions');

/* GET home page. */
router.get('/oprosnik', async function(req, res, next) {
  res.render('index', { title: 'Express'});
});

router.get('/questions', async function(req, res, next) {
  let questions = await getQuestions(path.join(__dirname, '../questions.csv'))
  res.send({questions});
});

router.post('/check-answers', async function(req, res, next) {
  let {answerNumbers} = req.body;
  let questions = await getQuestions(path.join(__dirname, '../questions.csv'))
  let mistakes = [];
 
  for (let i = 0; i < questions.length; i++) {
    //Проверям количесвто ответов пользователя с количеством правильных ответов
     if (questions[i].trueAnswers.length !== answerNumbers[i].length) {
      mistakes.push(`Ошибка в вопросе ${i}`)
      continue
    }


    //Получем из номером ответов текст ответов 

    let answers = []
    for (let answerNumber of answerNumbers[i]) {
      answers.push(questions[i].answers[answerNumber])
    };
    
    //Сверяем ответы 

    for (let j = 0; j < questions[i].trueAnswers.length; j++) {
      if (answers[j] !== questions[i].trueAnswers[j]) {
        mistakes.push(`Ошибка в вопросе ${i}, ответ ${j}`)
      }
    }
  }

  //Отправляем результаты

  res.send({
    success: !mistakes.length, 
    mistakes 
  });
});

module.exports = router;
